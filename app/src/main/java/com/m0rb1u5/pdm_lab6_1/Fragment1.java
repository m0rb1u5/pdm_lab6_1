package com.m0rb1u5.pdm_lab6_1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by m0rb1u5 on 20/02/16.
 */
public class Fragment1 extends Fragment{
   public Fragment1() {
   }

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
   }

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_1, container, false);
   }

   public static Fragment1 newInstance() {
      Fragment1 fragment = new Fragment1();
      return fragment;
   }
}
