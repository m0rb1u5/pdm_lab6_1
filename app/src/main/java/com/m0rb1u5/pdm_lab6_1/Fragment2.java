package com.m0rb1u5.pdm_lab6_1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by m0rb1u5 on 20/02/16.
 */
public class Fragment2 extends Fragment {
   public Fragment2() {
   }

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
   }

   @Nullable
   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      return inflater.inflate(R.layout.fragment_2, container, false);
   }

   public static Fragment2 newInstance() {
      Fragment2 fragment = new Fragment2();
      return fragment;
   }
}
